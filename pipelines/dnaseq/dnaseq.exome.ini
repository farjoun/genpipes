[DEFAULT]

java_other_options=-XX:+UseParallelGC -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304

experiment_type=exome
experiment_type_abrev=wes

[skewer_trimming]
threads=5

[bwa_mem]
other_options=-M -t 7

[picard_sort_sam]
ram=16G
max_records_in_ram=3750000

[bwa_mem_picard_sort_sam]
cluster_cpu=-l nodes=1:ppn=8

[picard_merge_sam_files]
ram=1700M
max_records_in_ram=250000

[gatk_indel_realigner]
nb_jobs=1
ram=6G
max_reads_in_memory=750000
other_options=-nt 1 -nct 1

[bvatools_groupfixmate]
ram=16G

[picard_mark_duplicates]
ram=10G
max_records_in_ram=2500000

[gatk_base_recalibrator]
threads=12
ram=55G
known_dbsnp=%(dbsnp)s
known_gnomad=%(gnomad_exome)s
known_mills=%(mills)s

[gatk_print_reads]
java_other_options=-XX:+UseParallelGC -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=4194304
threads=5
ram=24G

[gatk_apply_bqsr]
java_other_options=-XX:+UseParallelGC -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=4194304
options=
threads=5
ram=24G

[picard_collect_multiple_metrics]
ram=6G
max_records_in_ram=1000000
options=–-FILE_EXTENSION ".txt"

[picard_calculate_hs_metrics]
ram=6G
#baits_intervals=

[picard_collect_oxog_metrics]
ram=6G
max_records_in_ram=4000000

[picard_collect_gcbias_metrics]
ram=6G
max_records_in_ram=4000000

[dna_sample_qualimap]
ram=55G
use_bed=true

[dna_sambamba_flagstat]
flagstat_options=-t 3

[fastqc]
threads=3

[multiqc]
options=

[gatk_depth_of_coverage]
java_other_options=-XX:ParallelGCThreads=2
# maxDepth is RAM limited. maxDepth * 8 * nbIntervals ~= RAM needed
ram=8G
summary_coverage_thresholds=1,5,10,25,50,75,100,500

[bvatools_depth_of_coverage]
# You can set it implicitly, leave blank for whole genome or set auto which uses the sampleSheet to identify the bed file.
coverage_targets=auto
other_options=--gc --maxDepth 5001 --summaryCoverageThresholds 1,5,10,25,50,75,100,500,1000,2000,5000 --minMappingQuality 15 --minBaseQuality 15 --ommitN
ram=31G
threads=8

[igvtools_compute_tdf]
ram=6G
option=-f min,max,mean -w 25

[gatk_callable_loci]
ram=10G
# Usually you should put minDepthForLowMAPQ >= minDepth
other_options=-dt none --minDepth 10 --maxDepth 500 --minDepthForLowMAPQ 10 --minMappingQuality 10 --minBaseQuality 15

[bvatools_basefreq]
# Don't use the index, parse the whole file. Less RAM is needed this way
threads=0
ram=8G

[extract_common_snp_freq]
cluster_cpu=-l nodes=1:ppn=5

[bvatools_ratiobaf]
ram=70G
other_options=--plot --maxDepth 1000  --exclude %(excluded_chromosome)s

[baf_plot]
cluster_cpu=-l nodes=1:ppn=15

[gatk_haplotype_caller]
ram=30G
# Max is 1 per chromosome
nb_jobs=1

[gatk_cat_variants]
options=
ram=6G

[gatk_merge_and_call_individual_gvcfs]
options=-nt 1

[gatk_merge_and_call_combined_gvcfs]
options=-nt 1 -G StandardAnnotation -G StandardHCAnnotation -A FisherStrand -A QualByDepth -A ChromosomeCounts

[gatk_genotype_gvcf]
#-G AS_StandardAnnotation
ram=30G

[gatk_combine_gvcf]
ram=24G
nb_haplotype=88
nb_batch=1
other_options=

[merge_and_call_combined_gvcf]
ram=24G
java_other_options=-XX:+UseParallelGC -XX:ParallelGCThreads=2 -Dsamjdk.buffer_size=4194304
options=--useNewAFCalculator -G StandardAnnotation -G StandardHCAnnotation

[merge_and_call_individual_gvcf]
java_other_options=-XX:+UseParallelGC -XX:ParallelGCThreads=2 -Dsamjdk.buffer_size=4194304
ram=36G

[gatk_variant_recalibrator]
#Allele-specific annotation: -AS
options=-nt 11
ram=24G

[gatk_apply_recalibration]
options=-nt 11
ram=24G

[rawmpileup]
nb_jobs=25
mpileup_other_options=-d 1000 -B -q 1 -Q 0

[snp_and_indel_bcf]
approximate_nb_jobs=150
mpileup_other_options=-L 1000 -B -q 1 -t DP -t SP -g
bcftools_other_options=-O u -Avm

[snpsift_annotate]
ram=8G
java_other_options=-XX:ParallelGCThreads=2

[compute_effects]
java_other_options=-XX:ParallelGCThreads=1
ram=8G
options=-lof
snpeff_genome=hg19

[snpsift_dbnsfp]
ram=24G
java_other_options=-XX:ParallelGCThreads=2

[gemini_annotations]
options=-t snpEff --cores 11 --save-info-string

[verify_bam_id]
other_options=--verbose --ignoreRG --noPhoneHome

[report]
## Title for report e.g. <Project Name>
title=DNA-Seq Analysis Report

[manta_sv]
experiment_type_option=--exome

[cnvkit_batch]
batch_options=-m hybrid --short-names
fix_options=
segment_options=-m cbs -p 6
##call options can be changed to include purity & ploidy estimates if known a priori
threads=6

[svaba_run]
options=-p 5 -L 6 -I
cluster_walltime=-l walltime=24:00:0
cluster_cpu=-l nodes=1:ppn=6